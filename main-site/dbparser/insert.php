<?php

use PHPMailer\PHPMailer\PHPMailer;


header("Access-Control-Allow-Origin: *");

function ajaxJsonOutput($status='success',$msg='',$data=array())
{
	$output_data['status']=$status;
	$output_data['msg']=$msg;
	$output_data['data']=$data;
	echo json_Encode($output_data);
	die();
}


if($_POST['name'] && $_POST['email'])
{
    sendmail();
    ajaxJsonOutput();
}
else
ajaxJsonOutput('error');
function sendmail(){

require 'vendor/autoload.php';

	$mail = new PHPMailer;


	$mail->isSMTP();

	$mail->SMTPDebug = 0;

	$mail->Host = 'mail.ogilvylab.co.za';

	$mail->Port = 587;

	$mail->SMTPAuth = true;

	$mail->Username = 'hello@ogilvylab.co.za';

	$mail->Password = 'P@55w0rdP@55w0rd';

	$mail->setFrom('hello@ogilvylab.co.za', 'Ogilvy');

    $mail->addAddress('christophe.chantraine@ogilvy.co.za', 'Christophe');

	$mail->Subject = 'Social.Lab Website Contact';
	$message=file_get_contents('message.html');
	$message=str_replace("[Name]",$_POST['name'],$message);
	$message=str_replace("[Email]",$_POST['email'],$message);
    $message=str_replace("[Phone]",$_POST['phone'],$message);
    $message=str_replace("[Subject]",$_POST['subject'],$message);
    $message=str_replace("[Message]",$_POST['message'],$message);

	$mail->msgHTML($message);

	if ($mail->send())
		return true;

	return false;

}

