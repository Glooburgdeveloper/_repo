
<!-- for Google -->
<meta name="language" content="English">
<meta name="description" content="This is Social.Lab - Where creative excellence meets digital performance."/>
<meta name="keywords" content=""/>
<meta name="author" content="Ogilvy Lab South Africa" />
<meta name="copyright" content="Copyright ©Ogilvy. All Rights Reserved." />
<meta name="application-name" content="Social.Lab" />
<meta name="theme-color" content="#24298f">
<meta name="apple-mobile-web-app-status-bar-style" content="red">
<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1.0, user-scalable=no">

<link rel="icon" type="image/png" href="images/favicon.png" sizes="16x16">
<link rel="icon" type="image/png" href="images/favicon.png" sizes="32x32">
<link rel="icon" type="image/png" href="images/favicon.png" sizes="96x96">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script async src="js/html5shiv.js"></script>
<script async src="js/respond.min.js"></script>
<![endif]-->

<meta property="og:site_name" content="Social.Lab">
<meta name="twitter:image:alt" content="Share....">

<!-- Twitter Card -->
<meta name="twitter:site" content="@OgilvySA">
<meta name="twitter:title" content="Social.Lab">
<meta name="twitter:description" content="This is Social.Lab - Where creative excellence meets digital performance.">
<meta name="twitter:image" content="https://ogilvysociallab.co.za/images/SocialLab-ShareImage.jpg">
<meta name="twitter:card" content="summary_large_image">

<!-- for Facebook -->
<meta property="fb:app_id" content="2926298907642803" />
<meta property="og:title" content="Social.Lab"  />
<meta property="og:image" content="https://ogilvysociallab.co.za/images/SocialLab-ShareImage.jpg" />
<meta property="og:type" content="article" />
<meta property="og:url" content="https://ogilvysociallab.co.za" />
<meta property="og:description" content="This is Social.Lab - Where creative excellence meets digital performance." />

<link rel="prefetch" as="image" href="images/world-map.svg">

<link rel="stylesheet" type="text/css" media="screen" href="css/styles.css">






