<!-- Footer Section -->
<footer class="page-footer container-fluid">
    <div class="footer-container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 foot-info">
                <h1>Paris</h1>
                <p>
                    32-34 Rue Marbeuf
                    75008,
                    Paris France
                </p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 foot-info">
                <h1>London</h1>
                <p>
                    Sea Containers House 18 Upper Ground,
                    London, SE1 9RQ, UK
                </p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 foot-info">
                <h1>Amsterdam</h1>
                <p>
                    Amsteldijk 166
                    1079 LH,
                    Amsterdam Netherlands
                </p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 foot-info">
                <h1>New York</h1>
                <p>
                    636 11th Avenue
                    New York,
                    NY 10036 USA
                </p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 foot-info">
                <h1>Singapore</h1>
                <p>
                    71 Robinson Rd,
                    #07-01 Singapore,
                    068895 Singapore
                </p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 foot-info">
                <h1>Bucharest</h1>
                <p>
                    Str. Alexandru Grigore, 86 Bucuresti Sector 1,
                    10624 Romania
                </p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 foot-info">
                <h1>Hong Kong</h1>
                <p>
                    The Center
                    99 Queen’s Road,
                    Central Hong Kong
                </p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 foot-info">
                <h1>Cape Town</h1>
                <p>
                    41 Sir Lowry Rd, Woodstock 8000,
                    Cape Town, South Africa
                </p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 foot-info">
                <h1>Johannesburg</h1>
                <p>
                    The Brand Building, 15 Sloane Str.,
                    Bryanston, Johannesburg, 2152
                </p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 foot-info">
                <h1>Germany</h1>
                <p>
                    Rosenthaler Strasse 51 10178,
                    Berlin Germany
                </p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 foot-info">
                <h1>Switzerland</h1>
                <p>
                    Binzmühlestrasse 170d Postfach 6359,
                    8050 Zürich Switzerland
                </p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 foot-info">
                <h1>Poland</h1>
                <p>
                    Plac Konesera 8, 03-736,
                    Warszawa Polska
                </p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 foot-info">
                <h1>Dubai (Mena)</h1>
                <p>
                    Al Atar Business Tower, 26<sup>th</sup> floor,
                    Dubai International Financial Center,
                    Sheikh Zayed Road,
                    Dubai United Arab Emirates
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 foot-info">
                <a class="footer-logo" href="https://www.linkedin.com/showcase/social.lab-south-africa/" target="_blank">
                    <img src="images/logo-linkedin.svg" alt="LinkedIn">
                </a>
                <div class="links">
                    <a href="https://social-lab.eu/" target="_blank">Global Network</a>
                    <a href="https://social-lab.eu/privacy-policy/" target="_blank">Privacy Policy</a>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row copyright-bar">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 foot-info">
                <p class="copyright">
                    <span><span class="copy-icon">&copy;</span> Copyright Ogilvy Social Lab South Africa 2021. All Rights Reserved.</span>
                </p>
            </div>
        </div>
    </div>
</footer>
<!-- Default Scripts -->
<script type="text/javascript" src="js/jquery-2.2.1.min.js"></script>


