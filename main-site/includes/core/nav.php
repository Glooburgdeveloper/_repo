<div id="topPoint"></div>
<?php include 'mobilesplash.php'; ?>
<!-- page preloader -->
<div class="page-loader" id="page-loader">
<!--    <img class="logo" src="images/logo-ogilvy.svg"/>-->
    <div class="loader-bar">

    </div>
<!--    <div><span class="loading"></span><p>loading</p></div>-->
</div>
<div class="mobile-nav-btn loading">
    <span class="shape1"></span>
    <span class="shape3"></span>
</div>
<!-- navigation components -->
<div id="top-nav" class="loading">
    <div class="nav-box">
<!--        <div class="mobile-nav-btn">-->
<!--            <span class="shape1"></span>-->
<!--            <span class="shape2"></span>-->
<!--            <span class="shape3"></span>-->
<!--        </div>-->
        <a href="#AboutPage">
            <img class="logo" src="images/logo-social-lab.svg"/>
        </a>
        <nav id="mainNav" class="menu">
            <a href="#story" >Our Story</a>
            <a href="#clients" >Clients</a>
            <a href="#services" >Services</a>
            <a href="#footprint" >Footprint</a>
            <a href="#contact" >Contact</a>
            <a href="#careers" >Careers</a>
        </nav>
    </div>
</div>
<div id="sideNav">
    <a href="#AboutPage"><img class="logo" src="images/logo-social-lab.svg"/></a>
<!--    <div class="mobile-nav-btn active">-->
<!--        <span class="shape1"></span>-->
<!--        <span class="shape2"></span>-->
<!--        <span class="shape3"></span>-->
<!--    </div>-->
    <nav id="mainNav" class="menu">
        <a href="#story" >Our Story</a>
        <a href="#clients" >Clients</a>
        <a href="#services" >Services</a>
        <a href="#footprint" >Footprint</a>
        <a href="#contact" >Contact</a>
        <a href="#careers" >Careers</a>
    </nav>
</div>