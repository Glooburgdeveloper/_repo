
$ = jQuery;
let ua = navigator.userAgent;
let elemWidth = document.documentElement.clientWidth;
let elemHeight = document.documentElement.clientHeight;
let agent = navigator.userAgent.toLowerCase();
let mobileOS = typeof orientation != 'undefined' ? true : false;
let touchOS = ('ontouchstart' in document.documentElement) ? true : false;
let otherBrowser = (agent.indexOf("series60") != -1) || (agent.indexOf("symbian") != -1) || (agent.indexOf("windows ce") != -1) || (agent.indexOf("blackberry") != -1);
let iOS = (navigator.platform.indexOf("iPhone") != -1) || (navigator.platform.indexOf("iPad") != -1) ? true : false;
let isiPad = navigator.userAgent.match(/iPad/i);
let isMobile = navigator.userAgent.match(/Android|webOS|iPhone|iPod|BlackBerry|BB10|IEMobile|Opera Mini/i);
let isAndroidBased = (agent.indexOf("android") != -1) || (!iOS && !otherBrowser && touchOS && mobileOS) ? true : false;
let isAndroid = /android/.test(ua);
let isAndroidDefault = isAndroid && !(/chrome/i).test(ua);
let scrWidth = screen.width;
let scrHeight = screen.height;
let winHeight = $(window).outerHeight();
let isWindowsPhone = navigator.userAgent.match(/Windows Phone/i);
let is_chrome = navigator.userAgent.indexOf('Chrome') > -1;
let is_explorer = navigator.userAgent.indexOf('MSIE') > -1;
let is_firefox = navigator.userAgent.indexOf('Firefox') > -1;
let is_safari = /^((?!chrome).)*safari/i.test(navigator.userAgent);
let is_Opera = navigator.userAgent.indexOf("Presto") > -1;
let is_OldBrowser = false;
let aboveFoldView;
let offsetAmount;
let hash, checkHash;

/**
 * jQuery.browser.mobile (http://detectmobilebrowser.com/)
 *
 * jQuery.browser.mobile will be true if the browser is a mobile device
 *
 **/
(function(a){(jQuery.browser=jQuery.browser||{}).mobile=/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))})(navigator.userAgent||navigator.vendor||window.opera);

function getInternetExplorerVersion() {
    let rv = -1;
    if (navigator.appName == 'Microsoft Internet Explorer') {
        let ua = navigator.userAgent;
        let re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
        if (re.exec(ua) != null)rv = parseFloat(RegExp.$1);
    } else if (navigator.appName == 'Netscape') {
        let ua = navigator.userAgent;
        let re = new RegExp("Trident/.*rv:([0-9]{1,}[\.0-9]{0,})");
        if (re.exec(ua) != null)rv = parseFloat(RegExp.$1);
    }
    return rv;
}
function browserCheckInit() {
    let FF = !(window.mozInnerScreenX == null);
    if (/Edge\/12./i.test(navigator.userAgent)) {
        $('html').addClass('ieEdge');
    }
    if (getInternetExplorerVersion() === 11) {
        $('html').addClass('ie11');
    } else if (getInternetExplorerVersion() === 10) {
        $('html').addClass('ie10');
    } else if (getInternetExplorerVersion() === 9) {
        $('html').addClass('ie9');
    } else if (getInternetExplorerVersion() === 8) {
        $('html').addClass('ie8');
        modalInit('browser');
        is_OldBrowser = true;
    } else if (getInternetExplorerVersion() === 7) {
        $('html').addClass('ie7');
        modalInit('browser');
        is_OldBrowser = true;
    } else if (FF) {
        $('html').addClass('firefox');
    } else if (is_Opera) {
        $('html').addClass('opera');
    } else if (is_safari) {
        $('html').addClass('safari');
    }
    else if (isiPad) {
        $('html').addClass('iPad');
    }
}

function smoothy() {
    $('a[href*="#"]:not([href="#"])').on('click',function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            let target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({scrollTop: target.offset().top}, 500, "linear");
                return false;
            }
        }
    });
}

// site load defaults
function scrollDetect() {

    let scrollChange = false;
    $(window).scroll(function () {
        let scroll = $(window).scrollTop();

        // controls the menu minimised style
        if (isMobile != null && scrWidth < 480 || scrWidth < 640) {
            if (scroll >= 50) {
                $("#sideNav").addClass("minimal");
                if (scrollChange == false) {
                    scrollChange = true;
                }
                else {

                }

            } else {
                $("#sideNav").removeClass("minimal");
            }
        }
        else if (isiPad == true && scrWidth <= 768 || scrWidth <= 768) {
            if (scroll >= 50) {
                $("#sideNav").addClass("minimal");
                if (scrollChange == false) {
                    scrollChange = true;
                }
                else {

                }

            } else {
                $("#sideNav").removeClass("minimal");
            }
        }
        else {
            if (scroll >= 100) {
                $("#top-nav").addClass("minimal");
                $("#sideNav").addClass("minimal");
                if (scrollChange == false) {
                    scrollChange = true;
                }
                else {

                }
            } else {
                $("#top-nav").removeClass("minimal");
                $("#sideNav").removeClass("minimal");
                if (scrollChange == true) {
                    scrollChange = false;
                }
                else {

                }
            }
        }
    });
}
function mobileNav() {
    $('.mobile-nav-btn').on('click', function () {
        $('#sideNav, .overlayBG, #top-nav .social, #top-nav .mobile-nav-btn, #top-nav').toggleClass('active');
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $('#mainNav a').removeClass('active');
            $(this).addClass('active');
        }
    });
    $('.overlayBG').on('click', function () {
        $('#sideNav, .overlayBG, .mobile-nav-btn').removeClass('active');
    });
    $('#sideNav a, #sideNav .logo').on('click', function () {
        $('#sideNav, .overlayBG, .mobile-nav-btn').removeClass('active');
    });
}
function setDefaults() {
    aboveFoldView = winHeight / 2;
    offsetAmount = -300;

    // set active menu on page nav
    let currentPage = $('body').attr('id');
    switch (currentPage) {
        case "HomePage":
            $('.homePage').addClass('active');
            break;
    }

    if (isMobile != null && scrWidth <= 680 || scrWidth < 680) {
        $(window).on("orientationchange", function (event) {
            if (window.orientation == 90 || window.orientation == -90) {
                $('#rotateScreen').addClass('active');
            } else {
                $('#rotateScreen').removeClass('active');
            }
        });
    }
    else if (isMobile != null || isiPad != null && scrWidth < 800 || isMobile != null && scrWidth == 1280 || scrWidth < 1280) {
        $(window).on("orientationchange", function (event) {
            if (window.orientation == 90 || window.orientation == -90) {
                location.reload();
            } else {
                location.reload();
            }
        });

    }
    else {

    }

    if (!jQuery.browser.mobile) {
        jQuery('body').on('click', 'a[href^="tel:"]', function() {
            jQuery(this).attr('href',
                jQuery(this).attr('href').replace(/^tel:/, 'callto:'));
        });
    }
}
function setCountry(code){
    if(code || code==''){
        let text = jQuery('a[cunt_code="'+code+'"]').html();
        $(".dropdown dt a span").html(text);
    }

    $(".dropdown img.flag").addClass("flagvisibility");

    $(".dropdown dt a").on('click',function() {
        $(".dropdown dd ul").toggle();
    });

    $(".dropdown dd ul li a").click(function() {
        //console.log($(this).html())
        let text = $(this).attr('cunt_code');
        $(".dropdown dt a span").html(text);
        $(".dropdown dd ul").hide();
        $("#result").html("Selected value is: " + getSelectedValue("country-select"));
    });

    function getSelectedValue(id) {
        //console.log(id,$("#" + id).find("dt a span.value").html())
        return $("#" + id).find("dt a span.value").html();
    }

    $(document).bind('click', function(e) {
        let $clicked = $(e.target);
        if (! $clicked.parents().hasClass("dropdown"))
            $(".dropdown dd ul").hide();
    });


    $("#flagSwitcher").click(function() {
        $(".dropdown img.flag").toggleClass("flagvisibility");
    });
}
function serviceBg() {
    $('.service').off().on('mouseenter', function(){
        let type = $(this).attr('data-serviceType');
        switch (type) {
            case "digi-strat":
                $('#itemBg').removeClass();
                $('#itemBg').addClass(type);
                break;

            case "digi-media":
                $('#itemBg').removeClass();
                $('#itemBg').addClass(type);
                break;

            case "creation":
                $('#itemBg').removeClass();
                $('#itemBg').addClass(type);
                break;

            case "digi-intelli":
                $('#itemBg').removeClass();
                $('#itemBg').addClass(type);
                break;

            case "seo":
                $('#itemBg').removeClass();
                $('#itemBg').addClass(type);
                break;
        }
    });
}
function proximityImages() {
    // Helper lets and functions.
    const extend = function(a, b) {
        for( let key in b ) {
            if( b.hasOwnProperty( key ) ) {
                a[key] = b[key];
            }
        }
        return a;
    };

// from http://www.quirksmode.org/js/events_properties.html#position
    const getMousePos = function(ev) {
        let posx = 0;
        let posy = 0;
        if (!ev) ev = window.event;
        if (ev.pageX || ev.pageY) 	{
            posx = ev.pageX;
            posy = ev.pageY;
        }
        else if (ev.clientX || ev.clientY) 	{
            posx = ev.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
            posy = ev.clientY + document.body.scrollTop + document.documentElement.scrollTop;
        }
        return { x : posx, y : posy };
    };

    const TiltObj = function(el, options) {
        this.el = el;
        this.options = extend({}, this.options);
        extend(this.options, options);
        this.DOM = {};
        this.DOM.img = this.el.querySelector('.content__img');
        this.DOM.title = this.el.querySelector('.content__title');
        this._initEvents();
    };

    TiltObj.prototype.options = {
        movement: {
            img : { translation : {x: -10, y: -10} },
            title : { translation : {x: 25, y: 25} },
        }
    };

    TiltObj.prototype._initEvents = function() {
        this.mouseenterFn = (ev) => {
            anime.remove(this.DOM.img);
            anime.remove(this.DOM.title);
        };

        this.mousemoveFn = (ev) => {
            requestAnimationFrame(() => this._layout(ev));
        };

        this.mouseleaveFn = (ev) => {
            requestAnimationFrame(() => {
                anime({
                    targets: [this.DOM.img, this.DOM.title],
                    duration: 1500,
                    easing: 'easeOutElastic',
                    elasticity: 400,
                    translateX: 0,
                    translateY: 0
                });
            });
        };

        this.el.addEventListener('mousemove', this.mousemoveFn);
        this.el.addEventListener('mouseleave', this.mouseleaveFn);
        this.el.addEventListener('mouseenter', this.mouseenterFn);
    };

    TiltObj.prototype._layout = function(ev) {
        // Mouse position relative to the document.
        const mousepos = getMousePos(ev);
        // Document scrolls.
        const docScrolls = {left : document.body.scrollLeft + document.documentElement.scrollLeft, top : document.body.scrollTop + document.documentElement.scrollTop};
        const bounds = this.el.getBoundingClientRect();
        // Mouse position relative to the main element (this.DOM.el).
        const relmousepos = { x : mousepos.x - bounds.left - docScrolls.left, y : mousepos.y - bounds.top - docScrolls.top };

        // Movement settings for the animatable elements.
        const t = {
            img: this.options.movement.img.translation,
            title: this.options.movement.title.translation,
        };

        const transforms = {
            img : {
                x: (-1*t.img.x - t.img.x)/bounds.width*relmousepos.x + t.img.x,
                y: (-1*t.img.y - t.img.y)/bounds.height*relmousepos.y + t.img.y
            },
            title : {
                x: (-1*t.title.x - t.title.x)/bounds.width*relmousepos.x + t.title.x,
                y: (-1*t.title.y - t.title.y)/bounds.height*relmousepos.y + t.title.y
            }
        };
        this.DOM.img.style.WebkitTransform = this.DOM.img.style.transform = 'translateX(' + transforms.img.x + 'px) translateY(' + transforms.img.y + 'px)';
        this.DOM.title.style.WebkitTransform = this.DOM.title.style.transform = 'translateX(' + transforms.title.x + 'px) translateY(' + transforms.title.y + 'px)';
    };

    Array.from(document.querySelectorAll('.content--layout')).forEach(el => new TiltObj(el));
}
function scrollBgClr() {
    $(window).scroll(function() {

        // selectors
        let scroll;
        let $window = $(window),
            $body = $('body'),
            $panel = $('.panel-screen');

        // Change 33% earlier than scroll position so colour is there when you arrive.
        if (scrWidth < 769 ) {
            scroll = $window.scrollTop() + ($window.height() / 3);
        }

        if (scrWidth > 768 ) {
            scroll = $window.scrollTop() + ($window.height() / 3);
        }


        $panel.each(function () {
            let $this = $(this);

            // if position is within range of this panel.
            // So position of (position of top of div <= scroll position) && (position of bottom of div > scroll position).
            // Remember we set the scroll to 33% earlier in scroll let.
            if ($this.position().top <= scroll && $this.position().top + $this.height() > scroll) {

                // Remove all classes on body with color-
                $body.removeClass(function (index, css) {
                    return (css.match (/(^|\s)color-\S+/g) || []).join(' ');
                });

                // Add class of currently active div
                $body.addClass('color-' + $(this).data('color'));
            }
        });

    }).scroll();
}

// Form Controls
//contact checks
let uname,uemail,uphone, usubject, umessage = '';
function formCtrl(){

    // validate email
    function ValidateEmail(mail) {
        return !!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)
    }
    // check is number
    function isNumber(obj) {
        return !isNaN(parseFloat(obj))
    }

    $('.step').off().on('click', function(){
        //e.preventDefault();
        let inputsCount = $("#contactForm input").length + $("#contactForm textarea").length, inputsAll = $("#contactForm input, #contactForm textarea");
        let errors;
        $(".group").removeClass('invalid');
        $("label.error").remove();
        errors = inputsCount - 2;
        // console.log(inputsCount);
        // console.log(errors);

        jQuery.each(inputsAll, function () {
            let inputVal = $(this).val(), inputType = $(this).attr("class");
            let alpha = /^[a-zA-Z-,]+(\s{0,1}[a-zA-Z-, ])*$/;
            switch (inputType) {
                case"txt":
                    if(inputVal == "" || inputVal == " "){
                        $(this).parent().addClass('invalid');
                        $(this).parent().prepend('<label class="error">Please enter a valid name. <span class="icon"></span></label>');
                        //$(this).parent().find(".error").delay(1200).fadeOut(1200);
                    }
                    else {
                        if (inputVal.length > 2) {
                            errors--
                        }
                        else {
                            $(this).parent().addClass('invalid');
                            $(this).parent().prepend('<label class="error">This name is too short. <span class="icon"></span></label>');
                        }
                    }
                    break;

                case"subject":
                    if(inputVal == "" || inputVal == " "){
                        $(this).parent().addClass('invalid');
                        $(this).parent().prepend('<label class="error">Please enter a subject for message. <span class="icon"></span></label>');
                        //$(this).parent().find(".error").delay(1200).fadeOut(1200);
                    }
                    else {
                        if (/^[a-zA-Z-,]+(\s{0,1}[a-zA-Z-, ])*$/.test(inputVal)) {
                            if (inputVal.length > 2) {
                                errors--
                            }
                        }
                    }
                    break;

                case"message":
                    if(inputVal == "" || inputVal == " "){
                        $(this).parent().addClass('invalid');
                        $(this).parent().prepend('<label class="error">Please enter your message. <span class="icon"></span></label>');
                        //$(this).parent().find(".error").delay(1200).fadeOut(1200);
                    }
                    else {
                        if (inputVal.length > 2) {
                            errors--
                        }
                    }
                    break;

                case "email":
                    if(ValidateEmail(inputVal) == true){
                        errors--
                    }
                    else {
                        $(this).parent().addClass('invalid');
                        $(this).parent().prepend('<label class="error">Please enter a valid email address. e.g. joe.something@service.com <span class="icon"></span></label>');
                        //$(this).parent().find(".error").delay(1200).fadeOut(1200);
                    }
                    break;
                case "age":
                    if(isNumber(inputVal) == true){
                        if(inputVal.length == 2){

                            if(inputVal < 10 || inputVal > 99){
                                errors--
                            }
                        }
                        else {
                            $(this).parent().addClass('invalid');
                            $(this).parent().prepend('<label class="error">Please input a valid age. <span class="icon"></span></label>');
                            $(this).parent().find(".error").delay(1200).fadeOut(1200);
                        }
                    }
                    else {
                        $(this).parent().addClass('invalid');
                        $(this).parent().prepend('<label class="error">Please input a valid age. <span class="icon"></span></label>');
                        $(this).parent().find(".error").delay(1200).fadeOut(1200);
                    }
                    break;
                case "num":
                    if(isNumber(inputVal) == true){
                        if(inputVal.length == 10){
                            errors--
                        }
                        else {
                            $(this).parent().addClass('invalid');
                            $(this).parent().prepend('<label class="error">Please input a valid number. <span class="icon"></span></label>');
                            //$(this).parent().find(".error").delay(1200).fadeOut(1200);
                        }
                    }
                    else {
                        $(this).parent().addClass('invalid');
                        $(this).parent().prepend('<label class="error">Please input a valid number. <span class="icon"></span></label>');
                        //$(this).parent().find(".error").delay(1200).fadeOut(1200);
                    }
                    break;

            }
        });

        if(errors == 0 ){
            //console.log(errors);
            //console.log('clean');
            uname = $('#fullname').val();
            uemail = $('#email').val();
            uphone = '(' + $('#countryCode').html() + ') ' + $('#phone').val();
            usubject = $('#subject').val();
            umessage = $('#message').val();

            postData();
            //return false;
        }

    });

}
// send form data to db
function postData(){
    // save data
    $.ajax({
        url: 'https://ogilvylab.co.za/social-lab/dbparser/insert.php',
        type: 'POST',
        data: {
            'name': uname,
            'email': uemail,
            'phone': uphone,
            'subject': usubject,
            'message': umessage
        },
        success: function(response) {
            response = $.parseJSON(response);
            if (response.status == 'success') {
                $("#contactForm")[0].reset();
                $(".dropdown dt a span").html('+27');
                Swal.fire({
                    title: "Thank you!",
                    text: "You’re message has been successfully sent.",
                    icon: "success",
                    showConfirmButton: false
                });

            }
            else {
                Swal.fire({
                    title: "Sorry!",
                    text: "You’re message has not been sent, please try again later.",
                    icon: "error",
                    showConfirmButton: false
                });
            }
        }
    }).error(function(xhr, err, status) {
        Swal.fire({
            title: "Ooops",
            text: "There was a problem capturing your data, please try again.",
            icon: "error",
            showConfirmButton: false
        });
    });
}

document.addEventListener('DOMContentLoaded', () => {
    try {
        //alert(scrWidth);
        window.addEventListener("touchstart", function(e) {passive: true} );
        if ($("#AboutPage").length === 1) {
            let controller = new ScrollMagic.Controller({globalSceneOptions: {duration: aboveFoldView, offset:offsetAmount}});

            setTimeout(function(){
                $('#top-nav, .mobile-nav-btn').removeClass('loading');
                $('#page-loader').addClass('hidden');
                setTimeout(function(){
                    $('#page-loader').remove();

                    scrollDetect();
                    formCtrl();
                    serviceBg();
                    scrollBgClr();
                    setCountry();
                    if (scrWidth < 769 ) {
                        mobileNav();
                    }

                    if (scrWidth > 768 ) {
                        proximityImages();

                    }

                    new ScrollMagic.Scene({triggerElement: "#bio-about"})
                        .setClassToggle("#profile", "active")
                        .addTo(controller);
                    new ScrollMagic.Scene({triggerElement: "#map"})
                        .setClassToggle("#map", "active")
                        .addTo(controller);
                    new ScrollMagic.Scene({triggerElement: "#story"})
                        .setClassToggle("#story", "active")
                        .addTo(controller);
                    new ScrollMagic.Scene({triggerElement: "#storySystem"})
                        .setClassToggle("#storySystem", "active")
                        .addTo(controller);
                    new ScrollMagic.Scene({triggerElement: "#clients"})
                        .setClassToggle("#clients", "active")
                        .addTo(controller);
                    new ScrollMagic.Scene({triggerElement: "#services"})
                        .setClassToggle("#services", "active")
                        .addTo(controller);
                    new ScrollMagic.Scene({triggerElement: "#footprint"})
                        .setClassToggle("#footprint", "active")
                        .addTo(controller);
                    new ScrollMagic.Scene({triggerElement: "#contact"})
                        .setClassToggle("#contact", "active")
                        .addTo(controller);
                    new ScrollMagic.Scene({triggerElement: "#careers"})
                        .setClassToggle("#careers", "active")
                        .addTo(controller);
                }, 400);
            }, 1000);

        }
        // error page calls
        if ($("#ErrorPage").length === 1) {

        }
    } catch (err) {
        console.log(err);
    }
    getInternetExplorerVersion();
    browserCheckInit();
    smoothy();
    setDefaults();



});
