<!DOCTYPE html>
<html lang="en">
    <head>
    <meta charset='utf-8'>
    <title>Social.Lab</title>
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <?php include 'includes/core/meta.php'; ?>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-5P2K4GX');</script>
    <!-- End Google Tag Manager -->

    </head>
    <!-- NAVBAR
    ================================================== -->
    <body id="AboutPage">

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5P2K4GX"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->


    <?php include 'includes/core/nav.php'; ?>
        <!--
        <svg class="cursor" width="25" height="25" viewBox="0 0 25 25">
            <circle class="cursor__inner" cx="12.5" cy="12.5" r="6.25"/>
        </svg>
        -->
        <article id="bio-about" class="container-fluid">
            <div class="row" id="profile">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <picture>
                        <source media="(max-width:500px)" srcset="images/title-mobile.svg">
                        <img class="header-deco" src="images/title-desktop.svg" alt="">
                    </picture>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h4>
                        Do you want to join us?
                    </h4>
                    <a href="https://www.careers-page.com/ogilvysociallab" target="_blank" class="btn">View jobs</a>
                </div>
                <div id="socialLinks" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<!--                    <a href="https://www.facebook.com/carlaisfine" target="_blank" class="social facebook"></a>-->
<!--                    <a href="https://twitter.com/carlaisfine" target="_blank" class="social twitter"></a>-->
<!--                    <a href="https://www.linkedin.com/showcase/social.lab-south-africa/" target="_blank" class="social linkedin"></a>-->
                </div>
            </div>
        </article>

        <?php include 'includes/core/footer.php'; ?>

        <!-- Site specific JS -->
        <script type="text/javascript" src="js/scrollmagic/ScrollMagic.min.js"></script>
        <script type="text/javascript" src="js/script.js"></script>

    </body>
</html>
