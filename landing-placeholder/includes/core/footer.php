<!-- Footer Section -->
<footer class="page-footer container-fluid">
    <div class="footer-container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 foot-info">
                <p class="copyright">
                    <span><span class="copy-icon">&copy;</span> Copyright Ogilvy South Africa 2021. All Rights Reserved.</span>
                </p>
            </div>
        </div>
    </div>
</footer>
<!-- Default Scripts -->
<script type="text/javascript" src="js/jquery-2.2.1.min.js"></script>


