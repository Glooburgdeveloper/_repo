
<!-- for Google -->
<meta name="language" content="English">
<meta name="description" content=""/>
<meta name="keywords" content=""/>
<meta name="author" content="" />
<meta name="copyright" content="Copyright ©Ogilvy. All Rights Reserved." />
<meta name="application-name" content="Social.Lab" />
<meta name="theme-color" content="#F5BAC5">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1.0, user-scalable=no">

<link rel="icon" type="image/png" href="images/favicon.png" sizes="16x16">
<link rel="icon" type="image/png" href="images/favicon.png" sizes="32x32">
<link rel="icon" type="image/png" href="images/favicon.png" sizes="96x96">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script async src="js/html5shiv.js"></script>
<script async src="js/respond.min.js"></script>
<![endif]-->


<!-- Included in less build -->
<link rel="stylesheet" type="text/css" media="screen" href="css/styles.css">






